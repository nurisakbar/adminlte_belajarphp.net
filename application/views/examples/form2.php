<div class="row">
    <div class="col-md-12">
         <div class="box box-danger">
                                <div class="box-header">
                                    <h3 class="box-title">Different Width</h3>
                                </div>
                                <div class="box-body">
                                    
                                    <div class="row">
                                        <?php
                                        echo text('first_name', '', 'Nama Depan', 2, 'Masukan Nama Depan', '');
                                        ?>
                                        <?php
                                        echo text('last_name', '', 'Nama Belakang', 5, 'Masukan Nama Belakang', '');
                                        ?>
                                    </div>
                                    <br>
                                    <div class="row">
                                       <?php
                                        echo text('first_name', '', 'Nama Depan', 5, 'Masukan Nama Depan', '');
                                        ?>
                                        <?php
                                        combobox('service',4, 'Pilih Service','service', 'judul', 'id_service',6);
                                        ?>
                                        
                                    </div>
                                    <div class="row">
                                        <?php
                                        echo submit('<i class="fa  fa-desktop"></i> Daftar','btn bg-navy btn-flat margin');
                                        ?>
                                    </div>
                                    
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
    </div>
</div>