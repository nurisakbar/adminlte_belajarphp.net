<?php
// Created BY : Nuris akbar / nuris.akbar@gmail.com
function text($name=null,$value=null,$label=null,$class,$placeholder=null,$custome=array()){
    $default=array('name'=>$name,'value'=>$value,'placeholder'=>$placeholder,'class'=>'form-control');
    if($custome==''){
        $array=$default;
    }else{
        $array = array_merge($default,$custome);
    }
    return "<div class='col-xs-$class'><label>$label</label>".form_input($array)."</div>";
}

function submit($value,$class=null){
    return "<button type='submit' class='$class'>$value</button>";
}

function combobox($nama,$class,$label,$table,$field,$pk,$selected=null,$kondisi=null){
        $CI =& get_instance();
        if($kondisi==null)
        {
          $data=$CI->db->get($table)->result();  
        }
        else{ 
            $data=$CI->db->get_where($table,$kondisi)->result();
        }
        echo"<div class='col-xs-$class'>"
                . "<label>$label</label>"
                . "<select name='".$nama."' class='form-control'>";
        foreach ($data as $r)
        {
            echo" <option value='".$r->$pk."' ";
            echo $selected==$r->$pk?'selected':'';
            echo">".strtoupper($r->$field)."</option>";
        }
            echo"</select></div>";
    }
?>
